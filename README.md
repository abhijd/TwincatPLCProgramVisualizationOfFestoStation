# TwinCAT Festo Station Visualization and Control 

The task of this project was to use Twincat PLC programming software to write a program and create an automated and manual control visualization for the MPS Festo Distribution and Testing stations situation at the [FASTory  Lab](http://www.tut.fi/en/fast/fastory/learning-environments/fastorium/index.htm) of Tampere University of Technology.  

##Design 
For each station there are two Program Organization Unit (POU). One POU (name ends with “Control”) is for the control of different actuator in sequence and the other POU (name ends with “Sensors”) is to imitate the sensor outputs which is necessary for creating the visualization. To physically implement a real MPS Festo station only the first one needs to be used. Some timers were used in the first POU to imitate the real world working time of the different actuators. To use the first POU in real PLC all the timer may either be deleted or their times set to zero. In real application it will still work regardless of the timers because the program was carefully designed to be driven by sensor outputs i.e. each event is triggered by a specific sensor output. In  my  design  I  have  used  a  Boolean  variable  called  H_Meas  to  start  the  height measurement. In the visualization a button labelled “Imitate height Parameter” was used to switch toggle Work_OK. The light barrier sensors were not drawn in the visualization as because they are all static relative to their holder. 


**_Please look at thr readme.pdf file if you would like to only view the ladder diagrams without opening the project for help in ladder logic programming._**


## The Visualization

![picture alt](visu.png "Visualization of the FESTO line")

### How to run

* Install TwinCat 
* clone the repo in a local directory
* open the project from Twincat
* Once successfully loaded, compile the project.
* Once compiled without error, generate licence if necessary or else directly switch to run-mode
* In run-mode, log in and start after which the visualization can be used and controlled from the buttons in there.

## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is free to be used for any learning or educational purpose but cannot be used for commercial use without proper consent of the author.

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 